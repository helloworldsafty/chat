package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
	g := gin.Default()
    // 加载 template 目录下的模板文件
    g.LoadHTMLGlob("template/*")

    g.GET("/index", func(ctx *gin.Context) {   //跳转到主页面
        ctx.HTML(200, "index.html", nil)
    })

    g.POST("/sub",func(ctx *gin.Context) {   //提交数据表单
        ask := ctx.Query("ask")
        print(ask)  //测试数据
    })

    g.GET("/main", func(ctx *gin.Context) {   //跳转到主页面
        ctx.HTML(200, "main.html", nil)
    })

    g.Run(":8082")     //启动服务端口
}

